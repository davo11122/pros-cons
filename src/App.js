import React, { Component } from 'react';
import './App.css';

class Item extends Component{
	constructor(props){
		super(props);
		this.state = {item:props.source};
		this.onChangeContent = this.changeContent.bind(this);
		this.addSibling = this.addSibling.bind(this);
	}
	changeContent(event){
		var content = event.target.value;
		this.setState((prevState)=>{
			prevState.item.content = content;
			return {item:prevState.item}
		});
		if(!content){
			this.props.remove()
		}
		if(!content-1 && !this.state.item.content){
			this.props.addItem({indexed:this.props.index})
		}
	}
	addSibling(event){
		var content = event.target.value;
		if(event.key === "Enter" && content){
			this.props.addItem({content, index:this.props.index})
		}
	}
	render(){
		return (<li>
				<label>
					{this.props.index+1} .
					<input value={this.state.item.content} onChange={this.onChangeContent} onKeyPress={this.addSibling} ref={(input) => { this.itemInput = input }}/>
				</label>
			</li>)
	}
}
class Side extends Component{
	constructor(props){
		super(props);
		this.state = {items:[this.newItemInstance()	]}
		this.toAddItem = this.toAddItem.bind(this);
	}
	toAddItem(sourceItem){
		if(this.state.items.length-1 === sourceItem.index){
			if(sourceItem.content){
				this.addItem();
			}
		}else if(sourceItem.content){
			this.refs['input-'+(sourceItem.index+1)].itemInput.focus();
		}else if(sourceItem.indexed !== undefined && sourceItem.indexed === this.state.items.length-1 ){
			this.addItem()
		}
	}
	newItemInstance(){
		return {
			content:"",
			index:Date.now()*Math.random()
		}
	}
	addItem(){
		var item = this.newItemInstance();
		this.state.items.push(item);
		this.setState({items:this.state.items});
	}
	removeItemByIndex(index){
		return (function(){
			if(this.state.items.length !== index+1){
				this.state.items.splice(index,1);
				this.setState({items:this.state.items})
			}
		}).bind(this)
	}
	render(){
		var items = this.state.items.map((item, _index)=>(
			<Item key={item.index} source={item} index={_index} addItem={this.toAddItem} remove={this.removeItemByIndex(_index)} ref={"input-" + _index}/>));
		return (<div className={"side-"+this.props.name.toLocaleLowerCase()}>
						<div className="title"><h2><span className={this.props.isFake && "fake"}>{this.props.name}</span>{this.props.isFake && "Pros"}</h2></div>
						<div>
							<ul>
								{items}
							</ul>
						</div>
					</div>)
	}
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Should we hire Davit?</h2>
        </div>
				<div className="App-body">
					<Side name="Pros" />
					<Side name="Cons" isFake={true}/>
				</div>
			</div>
    );
  }
}

export default App;
